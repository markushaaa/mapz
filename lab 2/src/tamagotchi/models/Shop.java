package tamagotchi.models;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.LevelFactory.Flyweight;
import tamagotchi.models.Observer.FoodProvider;
import tamagotchi.models.Observer.Observed;
import tamagotchi.models.Observer.Observer;
import tamagotchi.models.food.Food;
import tamagotchi.models.food.HarmfulFoodDecorator;
import tamagotchi.models.food.HealthyFoodDecorator;
import tamagotchi.models.food.IFood;

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Shop implements Serializable, Observed {
    private static final long serialVersionUID = 1L;
    public static ImageIcon shopImage = ImageFactory.getImage(Image.SHOP);
    public static int dx = 5;
    public static int dy = 330;
    public static int width = 150;
    public static int height = 150;
    private static Shop shop;
    private ArrayList<IFood> foods;
    private ArrayList<Observer> observers;
    private Shop(){
        foods = new ArrayList<IFood>();
        observers = new ArrayList<Observer>();
        addObserver(new FoodProvider());
        initializeShop();
    }
    public void addHealthyFood(Food food)
    {
        HealthyFoodDecorator healthyFood = new HealthyFoodDecorator(food);
        foods.add(healthyFood);
    }
    public void addHarmfulFood(Food food){
        HarmfulFoodDecorator harmfulFood = new HarmfulFoodDecorator(food);
        foods.add(food);
    }
    public void removeFood(IFood food){
        foods.remove(food);
    }
    public ArrayList<IFood> getFoods(){
        return foods;
    }
    public static Shop getInstance(){
        if(shop == null){
            shop = new Shop();
        }
        return shop;
    }
    public void initializeShop(){
        for(int i = 0; i < 4;i++){
            Food apple = new Food(Flyweight.getAppleSprite(),"Apple",10,10);
            addHealthyFood(apple);
        }
        for(int i = 0; i < 4;i++){
            Food persic = new Food(Flyweight.getPersicSprite(),"Peach",10,10);
            addHarmfulFood(persic);
        }
    }

    public boolean isEmpty(){
        return foods.isEmpty();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer:observers) {
            observer.handleEvent(this);
        }
    }
}
