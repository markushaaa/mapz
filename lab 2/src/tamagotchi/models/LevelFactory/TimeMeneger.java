package tamagotchi.models.LevelFactory;

import tamagotchi.models.Pet;

import javax.management.timer.TimerMBean;
import javax.swing.*;
import java.io.Serializable;

public interface TimeMeneger extends Serializable {
    void reduceSatiety();
    void reduceHealth();
    void increaseXp();
    void startTimer();
    void setPet(Pet pet);
    String toString();
}
