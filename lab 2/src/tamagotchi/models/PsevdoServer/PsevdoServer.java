package tamagotchi.models.PsevdoServer;

import tamagotchi.models.food.IFood;

import java.util.ArrayList;

public class PsevdoServer {
    private ArrayList<IFood> PurchaseList;

    public PsevdoServer() {
        PurchaseList = new ArrayList<IFood>();
    }

    public void addPurchase(IFood purchase){
        PurchaseList.add(purchase);
    }

    public void deletePurchase(){
        PurchaseList.remove(PurchaseList.size() - 1);
    }

    public ArrayList<IFood> getPurchases(){
        return PurchaseList;
    }

    public void deleteAllPurchesases(){
        PurchaseList.clear();
    }
}
