package tamagotchi.models.PsevdoServer;

public class DeleteAllCommand implements Command{
    private PsevdoServer ps;

    public DeleteAllCommand(PsevdoServer ps) {
        this.ps = ps;
    }

    @Override
    public void execute() {
        ps.deleteAllPurchesases();
    }
}
