package tamagotchi.models.PsevdoServer;

import tamagotchi.models.food.IFood;

public class DeleteCommand implements Command {
    private PsevdoServer ps;


    public DeleteCommand(PsevdoServer ps) {
        this.ps = ps;
    }

    @Override
    public void execute() {
        ps.deletePurchase();
    }

}
