package tamagotchi.models;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.PetStates.*;
import javax.swing.*;
import java.io.Serializable;

public class Pet implements Serializable {
    private static final long serialVersionUID = 1L;
    public static ImageIcon petImage = ImageFactory.getImage(Image.PET);
    public static int dx = 240;
    public static int dy = 240;
    private int eat = 100;
    private int health = 100;
    private int xpPoints = 0;
    private int gold = 0;
    private String name;
    private State state;
    private Storage storage;
    private volatile static Pet pet = null;
    private Pet(String name){
        this.name = name;
        storage = Storage.getInstance();
    }

    public static synchronized Pet getInstance(String name){
        if(pet == null){
            synchronized (Pet.class){
                if(pet == null){
                    pet = new Pet(name);
                }
            }
        }
        return pet;
    }

    public void checkState(){
        if(health >= 50){
            state = new FunnyState();
        }
        else if(health > 0 && health < 50){
            state = new SickState();
        }
        else{
            state = new DeathState();
        }
        state.setImage(petImage);
    }

    public void treat(int medicine){
        if(health < 100){
            health += medicine;
            if(health> 100){
                health = 100;
            }
        }
        else {
            System.out.println("I am OK");
        }
    }

    public String getName() {
        return name;
    }

    public int getGold() {
        return gold;
    }

    public int getHealth(){
        return health;
    }

    public int getEat(){
        return eat;
    }

    public int getXpPoints() {
        return xpPoints;
    }

    public void changeGold(int delta){
        this.gold += delta;
    }

    public void changeHealth(int delta) {
        this.health += delta;
        if (health < 0) {
            health = 0;
        }
    }

    public void changeSatiaty(int delta){
        this.eat += delta;
        if(eat < 0){
            eat = 0;
        }
    }

    public void changeXp(int delta){
        xpPoints += delta;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "eat=" + eat +
                ", health=" + health +
                ", xpPoints=" + xpPoints +
                ", gold=" + gold +
                ", name='" + name + '\'' +
                '}';
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }
}
