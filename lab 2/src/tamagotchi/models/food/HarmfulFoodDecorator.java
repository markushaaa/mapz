package tamagotchi.models.food;

import tamagotchi.models.Pet;

public class HarmfulFoodDecorator extends BaseFoodDecorator {

    public HarmfulFoodDecorator(Food food) {
        super(food);
    }

    @Override
    public void use(Pet pet) {
        super.use(pet);
        pet.changeHealth(-10);
    }
}
