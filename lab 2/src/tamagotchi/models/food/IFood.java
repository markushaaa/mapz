package tamagotchi.models.food;

import tamagotchi.models.Pet;

import javax.swing.*;
import java.io.Serializable;

public interface IFood extends Serializable {
    void use(Pet pet);
    ImageIcon getImage();
    IFood clone();
}
