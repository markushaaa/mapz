package tamagotchi.models;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.food.IFood;
import tamagotchi.models.Pet;
import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;

public class Storage implements Serializable {
    private static final long serialVersionUID = 1L;
    public static ImageIcon storageImage = ImageFactory.getImage(Image.STORAGE);
    public static int dx = 500;
    public static int dy = 350;
    public static int width = 130;
    public static int height = 130;
    private static Storage storage;
    private int capasity = 10;
    private int available = 10;
    private  ArrayList<IFood> foods;
    private Storage(){
        foods = new ArrayList<IFood>(capasity);
    }

    public static Storage getInstance(){
        if(storage == null) {
            storage = new Storage();
        }
        return storage;
    }

    void useItem(IFood food){
        food.use(Pet.getInstance("fds"));
        removeFood(food);
    }

    void addFood(IFood food){
        foods.add(food);
        available--;
    }

    public void removeFood(IFood food){
        foods.remove(food);
        available++;
    }

    public int getCapasity(){
        return capasity;
    }

    public boolean isFreeSpace(){
        return available > 0;
    }

    public ArrayList<IFood> getFoods(){
        return foods;
    }

    @Override
    public String toString() {
        if(foods != null) {
            return "Storage{" +
                    "available=" + available +
                    "Foods= " + foods.toString() +
                    '}';
        }
        else {
            return "Storage{" +
                    "available=" + available +
                    '}';
        }
    }
}
