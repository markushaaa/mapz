package tamagotchi.models.food;

import tamagotchi.models.Pet;

import java.io.Serializable;

public class HealthyFoodDecorator extends BaseFoodDecorator {

    public HealthyFoodDecorator(Food food) {
        super(food);
    }

    @Override
    public void use(Pet pet) {
        super.use(pet);
        pet.changeHealth(10);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
