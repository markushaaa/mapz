package tamagotchi.models.LevelFactory;

import tamagotchi.models.Pet;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HardTimeMeneger extends Timer implements TimeMeneger{
    public ActionListener actions = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            reduceSatiety();
            reduceHealth();
            increaseXp();
        }
    };
    Pet pet;
    int delay = 6000;
        public HardTimeMeneger() {
            super(0,null);
            setDelay(delay);

            addActionListener(actions);
        }

    @Override
    public void reduceSatiety() {
        System.out.println("Reduce Satiety");
    }

    @Override
    public void reduceHealth() {
        System.out.println("Reduce health");
    }

    @Override
    public void increaseXp() {
        System.out.println("Increase xp");
    }

    @Override
    public void startTimer() {
        super.start();
    }

    @Override
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString(){
            return "Hard Timer";
    }
}
