package tamagotchi.models.LevelFactory;

import java.io.Serializable;

public interface LevelMenager extends Serializable {
    public abstract int ConvertXpValueToPercent(int xpValue);
    public abstract int ConvertXpValueToLevel(int xpValue);
    public abstract String toString();
}
