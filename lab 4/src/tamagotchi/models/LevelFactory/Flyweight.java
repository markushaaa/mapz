package tamagotchi.models.LevelFactory;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;

import javax.swing.*;

public  class Flyweight {
    private static ImageIcon appleSprite = ImageFactory.getImage(Image.APPLE);
    private static ImageIcon persicSprite = ImageFactory.getImage(Image.PERSIC);
    public static ImageIcon getAppleSprite(){
        return appleSprite;
    }
    public static ImageIcon getPersicSprite(){
        return persicSprite;
    }
}
