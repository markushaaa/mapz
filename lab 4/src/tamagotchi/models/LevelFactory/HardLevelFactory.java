package tamagotchi.models.LevelFactory;

public class HardLevelFactory extends DifficultyLevelFactory {
    @Override
    public LevelMenager getLevelMenager() {
        return new HardLevelMeneger();
    }

    @Override
    public TimeMeneger getTimeMenager() {
        return new HardTimeMeneger();
    }
}
