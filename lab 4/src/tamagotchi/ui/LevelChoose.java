package tamagotchi.ui;

import tamagotchi.constants.Constants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class LevelChoose {
    private JPanel panel1;
    private JButton easyButton;
    private JButton hardButton;
    private JTextField PetNameTF;
    private JLabel NameError;

    public LevelChoose() {
        NameError.hide();
        panel1.setPreferredSize(new Dimension(Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT));
        easyButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String petName = PetNameTF.getText();
                if(petName.isEmpty()){
                    NameError.show();
                    return;
                }
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(panel1);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                try {
                    topFrame.add(GamePanel.getInstance("Easy",petName,false), BorderLayout.CENTER);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                topFrame.pack();
            }
        });
        hardButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String petName = PetNameTF.getText();
                if(petName.isEmpty()){
                    NameError.show();
                    return;
                }
                JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(panel1);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                try {
                    topFrame.add(GamePanel.getInstance("Hard",petName,false), BorderLayout.CENTER);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                topFrame.pack();
            }
        });
    }
     public JPanel getPanel1(){
        return panel1;
    }


}
