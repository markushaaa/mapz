package tamagotchi.images;

import tamagotchi.constants.Constants;

import javax.swing.*;
import java.awt.*;

public class ImageFactory {
    private ImageFactory(){}
    public static ImageIcon getImage(Image img){
        switch (img){
            case PET:
                return new ImageIcon(Constants.PET_IMAGE);

            case ROOM:
                return new ImageIcon(Constants.ROOM_IMAGE);

            case DESK:
                return new ImageIcon(Constants.DESC_IMAGE);

            case EAT_STATUS:
                return new ImageIcon(Constants.FOOD_IMAGE);

            case LEVEL_STATUS:
                return new ImageIcon(Constants.LEVEL_IMAGE);

            case HP_STATUS:
                return new ImageIcon(Constants.HEALTH_IMAGE);

            case SHOP:
                return new ImageIcon(Constants.SHOP_IMAGE);

            case STORAGE:
                return new ImageIcon(Constants.STORAGE_IMAGE);

            case BACK_CHANGE:
                return new ImageIcon(Constants.BACK_CHANGE_IMAGE);

            case GOLD:
                return new ImageIcon(Constants.COIN_IMAGE);

            case APPLE:
                return new ImageIcon(Constants.APPLE_IMAGE);

            case GRUSHA:
                return new ImageIcon(Constants.GRUSHA_IMAGE);

            case KLUBNIKA:
                return new ImageIcon(Constants.KLUBNIKA_IMAGE);

            case ORANGE:
                return new ImageIcon(Constants.ORANGE_IMAGE);

            case PERSIC:
                return new ImageIcon(Constants.PERSIK_IMAGE);

            case SAVE:
                return new ImageIcon(Constants.SAVE_IMAGE);

            case SICK_PET:
                return new ImageIcon(Constants.SICK_PET_IMAGE);

            case DEAD_PET:
                return new ImageIcon(Constants.DEATH_PET_IMAGE);

            case LIST:
                return new ImageIcon(Constants.PURCHASE_LIST_IMAGE);
        }
        return null;
    }
}
