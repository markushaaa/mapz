package tamagotchi.constants;

public  class Constants {
    private Constants(){

    }

    public static final String TITLE = "Tamagothi";

    public static final int BOARD_WIDTH = 640;

    public static final int BOARD_HEIGHT = 480;

    public static final String PET_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\main\\purpleSprite.png";

    public static final String ROOM_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\main\\background.png";

    public static final String DESC_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\main\\desk.png";

    public static final String COIN_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\stats\\coin.png";

    public static final String FOOD_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\stats\\food.png";

    public static final String HEALTH_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\stats\\hearts.png";

    public static final String LEVEL_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\stats\\level.png";

    public static final String SHOP_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\actions\\buyImage.png";

    public static final String STORAGE_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\actions\\store.png";

    public static final String BACK_CHANGE_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\stats\\changeBackground.png";

    public static final String APPLE_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\food\\apple.png";

    public static final String GRUSHA_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\food\\grusha.png";

    public static final String KLUBNIKA_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\food\\klubnika.png";

    public static final String ORANGE_IMAGE = "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\food\\orange.png";

    public static final String PERSIK_IMAGE= "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\food\\persik.png";

    public static final String SAVE_IMAGE= "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\actions\\save.png";

    public static final String SICK_PET_IMAGE= "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\main\\SickSprite.png";

    public static final String DEATH_PET_IMAGE= "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\main\\DeadSprite.png";

    public static final String PURCHASE_LIST_IMAGE= "D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\image\\actions\\PurchaseList.png";



    public static final int GAME_SPEED = 10;
}
