#pragma once
#ifndef VARIABLES_H
#define VARIABLES_H
#include <QMap>

class Variables
{
    private:
    static QMap<QString,double> variables;
public:
    Variables();


    static double get(QString key);


    static bool isExist(QString key);


    static void set(QString key,double val);

    static QString toString(){
        QString result;
        for(auto iter = variables.begin();iter != variables.end();iter++){
            result.append("key - " + iter.key() + " val - " + QString::number(iter.value()));
        }
        return result;
    }
    static QMap<QString, double> getVariables();
    static void setVariables(const QMap<QString, double> &value);
};


#endif // VARTABLE_H
