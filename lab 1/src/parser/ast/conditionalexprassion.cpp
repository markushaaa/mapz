#include "conditionalexprassion.h"



Exprassion *ConditionalExprassion::getExpr1() const
{
    return expr1;
}



Exprassion *ConditionalExprassion::getExpr2() const
{
    return expr2;
}



void ConditionalExprassion::setExpr1(Exprassion *value)
{
    expr1 = value;
}

void ConditionalExprassion::setExpr2(Exprassion *value)
{
    expr2 = value;
}

ConditionalExprassion::ConditionalExprassion(char operation, Exprassion *expr1, Exprassion *expr2){
    this->operation = operation;
    this->expr1 = expr1;
    this->expr2 = expr2;
}

Value* ConditionalExprassion::eval(){
    double val1 = expr1->eval()->asNumber();
    double val2 = expr2->eval()->asNumber();
    switch (operation) {
    case '<' : return new NumberValue(val1 < val2);
    case '>' : return new NumberValue(val1 > val2);
    case '=' :
    default: return new NumberValue(val1 == val2);
    }
}

void ConditionalExprassion::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* operationItem = new QTreeWidgetItem;
    operationItem->setText(0,QString(operation));
    if(father != nullptr){
        father->addChild(operationItem);
    }
    else{
        AST->addTopLevelItem(operationItem);
    }
    expr1->toString(AST,operationItem);
    expr2->toString(AST,operationItem);
}
