#pragma once
#ifndef STRINGEXPRASSION_H
#define STRINGEXPRASSION_H
#include "exprassion.h"

class StringExprassion : public Exprassion
{
private:
    QString value;
public:

    StringExprassion(QString value);

    // Exprassion interface
public:
    Value *eval();
    void toString(QTreeWidget*,QTreeWidgetItem*);


    // Node interface
    Node* accept(Visitor* visitor){
        return this;
    }
};

#endif // STRINGEXPRASSION_H
