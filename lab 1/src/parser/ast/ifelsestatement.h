#pragma once
#ifndef IFELSESTATEMENT_H
#define IFELSESTATEMENT_H
#include "statement.h"
#include "exprassion.h"
#include "breakstatement.h"
class IfElseStatement: public Statement
{
private:
    Statement *ifStatement,*elseStatement;
    Exprassion *expr;
public:
    IfElseStatement(Exprassion* expr,Statement* ifStatement,Statement* elseStatement);
    // Statement interface
public:
    void execute();
    void toString(QTreeWidget*, QTreeWidgetItem*);

    // Node interface
    Node* accept(Visitor* visitor){
        return visitor->visit(this);
    }
    Statement *getIfStatement() const;
    Statement *getElseStatement() const;
    Exprassion *getExpr() const;
    void setIfStatement(Statement *value);
    void setElseStatement(Statement *value);
    void setExpr(Exprassion *value);
};

#endif // IFELSESTATEMENT_H
