#pragma once
#ifndef CLICKFUNCTION_H
#define CLICKFUNCTION_H
#include "functions.h"
#include "function.h"
#include "QList"
#include <QDebug>
#include "clickthread.h"


class ClickFunction : public Function
{
public:
    ClickFunction();

    // Function interface
public:
    Value* execute(QList<Value*> args);
};

#endif // CLICKFUNCTION_H
