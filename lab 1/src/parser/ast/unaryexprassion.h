#pragma once
#ifndef UNARYEXPRASION_H
#define UNARYEXPRASION_H
#include "exprassion.h"

class UnaryExprassion: public Exprassion
{
private:
    Exprassion* expr;
    char operation;
public:
    UnaryExprassion(char operation,Exprassion* expr);



    // Exprassion interface
public:
    Value* eval();
    void toString(QTreeWidget* , QTreeWidgetItem*);

    // Node interface
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    Exprassion *getExpr() const;
    char getOperation() const;
};

#endif // UNARYEXPRASION_H
