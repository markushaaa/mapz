#include "stringexprassion.h"


StringExprassion::StringExprassion(QString value){
    this->value = value;
}

Value *StringExprassion::eval(){
    return new StringValue(value);
}

void StringExprassion::toString(QTreeWidget* AST, QTreeWidgetItem* father)
{
    QTreeWidgetItem* item = new QTreeWidgetItem;
    item->setText(0,value);
    if(father != nullptr){
        father->addChild(item);
    }
    else{
        AST->addTopLevelItem(item);
    }
}
