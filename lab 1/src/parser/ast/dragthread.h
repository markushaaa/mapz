#pragma once
#ifndef DRAGTHREAD_H
#define DRAGTHREAD_H
#include <QThread>
#include <QList>
#include "value.h"
#include "windows.h"
#include <QDebug>
class DragThread : public QThread
{
    QList<Value*> args;
public:
    DragThread(QList<Value*> args);

    // QThread interface
protected:
    void run();
};

#endif // DRAGTHREAD_H
