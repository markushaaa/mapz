#pragma once
#ifndef VARIABLEEXPRESSION_H
#define VARIABLEEXPRESSION_H
#include "exprassion.h"
#include "variables.h"
class VariableExpression: public Exprassion
{
private:
    QString name;
public:
    VariableExpression(QString name);

    // Exprassion interface
public:
    Value* eval();
    void toString(QTreeWidget* , QTreeWidgetItem*);

    // Node interface
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    QString getName() const;
};

#endif // VARIABLEEXPRESSION_H
