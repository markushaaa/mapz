#include "assignmentstatement.h"

Exprassion *AssignmentStatement::getExpr() const
{
    return expr;
}

QString AssignmentStatement::getVar() const
{
    return var;
}

void AssignmentStatement::setExpr(Exprassion *value)
{
    expr = value;
}

AssignmentStatement::AssignmentStatement(QString var, Exprassion *expr){
    this->var = var;
    this->expr = expr;
}

void AssignmentStatement::execute(){
    double result = expr->eval()->asNumber();
    Variables::set(var,result);

}

void AssignmentStatement::toString(QTreeWidget *AST, QTreeWidgetItem *father){
    QTreeWidgetItem* itm = new QTreeWidgetItem;
    itm->setText(0,":(Assignment)");
    if(father != nullptr){
        father->addChild(itm);
    }
    else{
        AST->addTopLevelItem(itm);
    }
    QTreeWidgetItem* varItem = new QTreeWidgetItem;
    varItem->setText(0,var);
    itm->addChild(varItem);
    expr->toString(AST,itm);
}

