#include "parser.h"


Token Parser::get(int relPos){
    int position = relPos + pos;
    if(position > tokens.length()){
        return Token(Token::tokenType::EoF,"/0");
    }
    return tokens[position];
}

bool Parser::eat(Token::tokenType type){
    Token curToken = get(0);
    if(curToken.getType() == type){
        pos++;
        return true;
    }
    return false;
}

Statement *Parser::statement(){
    if(eat(Token::tokenType::WHILE)){
        eat(Token::tokenType::SEPARATORDP);
        return whileStatement();
    }
    if(get(0).getType() == Token::tokenType::CLICK || get(0).getType() == Token::tokenType::PRESS
               || get(0).getType() == Token::tokenType::RUN || get(0).getType() == Token::tokenType::DRAG){
       return function();
    }
    if(eat(Token::tokenType::IF)){
        eat(Token::tokenType::SEPARATORDP);
        return ifElse();
    }
    if(eat(Token::tokenType::BREAK)){
        return new BreakStatement;
    }
    return assignmentStatement();
}



Statement *Parser::assignmentStatement(){
    if(get(0).getType() == Token::tokenType::VAR && get(1).getType() == Token::tokenType::SEPARATORDP){
        QString var = get(0).getValue();
        if(eat(Token::tokenType::VAR)){
            if(eat(Token::tokenType::SEPARATORDP)){
                Exprassion* expr = exprasion();
                Statement* statemenet = new AssignmentStatement(var,expr);
                return statemenet;
            }
        }
    }
    if(get(0).getType() == Token::tokenType::VAR){
        throw ("Missing :");
    }
}

Statement *Parser::ifElse(){
        Exprassion* condition = exprasion();
        Statement* ifStatement;
        if(eat(Token::tokenType::LBRACE)){
            ifStatement = block();
        }
        else{
            ifStatement = statement();
        }
        Statement* elseStatement = nullptr;
        if(eat(Token::tokenType::ELSE)){
            if(eat(Token::tokenType::LBRACE)){
                elseStatement = block();
            }
            else{
                elseStatement = statement();
            }
        }
        return new IfElseStatement(condition,ifStatement,elseStatement);
}

Statement *Parser::block()
{
    BlockStatement* block = new BlockStatement;
    while((!eat(Token::tokenType::RBRACE)) && (!eat(Token::tokenType::EoF))){
          block->add(statement());
          eat(Token::tokenType::SEPARATORPC);
    }
    return block;
}

Exprassion *Parser::exprasion(){
    return condition();
}


Exprassion *Parser::condition()
{
    Exprassion* result = additive();
    while(true){
        if(eat(Token::tokenType::GS)){
            return new ConditionalExprassion('>',result,additive());
        }

        else if(eat(Token::tokenType::LS)){
            return new ConditionalExprassion('<',result,additive());
        }

        if(eat(Token::tokenType::EQUALLY)){
            return new ConditionalExprassion('=',result,additive());
        }
        break;
    }
    return result;
}

Statement *Parser::function()
{
    QString name;
    if(get(0).getType() == Token::tokenType::CLICK){
       name = consume(Token::tokenType::CLICK).getValue();
    }

    else if(get(0).getType() == Token::tokenType::PRESS){
        name = consume(Token::tokenType::PRESS).getValue();
    }

    else if(get(0).getType() == Token::tokenType::RUN){
        name = consume(Token::tokenType::RUN).getValue();
    }

    else if(get(0).getType() == Token::tokenType::DRAG){
        name = consume(Token::tokenType::DRAG).getValue();
    }

    consume(Token::tokenType::SEPARATORDP);
    FunctionExpression* functionExp = new FunctionExpression(name);
    while(!eat(Token::tokenType::SEPARATORPC)){
        functionExp->addArgument(exprasion());
        eat(Token::tokenType::COMA);
    }
    return new FunctionStatement(functionExp);
}

Statement *Parser::whileStatement()
{
    Exprassion* condition = exprasion();
    Statement* body;
    if(eat(Token::tokenType::LBRACE)){
        body = block();
    }
    else{
        body = statement();
    }
    return new WhileStatement(condition,body);
}

Token Parser::consume(Token::tokenType type)
{
    Token curToken = get(0);
    if(curToken.getType() == type){
        pos++;
        return curToken;
    }
    else{
        if(type == Token::tokenType::SEPARATORDP){
            throw "Missing ':'";
        }
        else if(type == Token::tokenType::SEPARATORPC){
            throw "Missing ';'";
        }
        else if(type == Token::tokenType::RBRACKET){
            throw "Missing ')'";
        }
        else if(type == Token::tokenType::RBRACE){
            throw "Missing '}'";
        }
    }
}


Exprassion *Parser::additive(){
    Exprassion* result = multiplicative();
    while(true){
        if(eat(Token::tokenType::PLUS)){
            result = new BinaryExprassion('+', result, multiplicative());
            continue;
        }
        else if(eat(Token::tokenType::MINUS)){
            result = new BinaryExprassion('-', result, multiplicative());
            continue;
        }
        break;
    }
    return result;
}

Exprassion *Parser::multiplicative(){
    Exprassion* result = unary();
    while(true){
        if(eat(Token::tokenType::MUL)){
            result = new BinaryExprassion('*',result, unary());
            continue;
        }
        else if(eat(Token::tokenType::DIV)){
            result = new BinaryExprassion('/',result, unary());
            continue;
        }
        break;
    }
    return result;
}

Exprassion *Parser::unary(){
    if(eat(Token::tokenType::PLUS)){
        return new UnaryExprassion('+',primary());
    }
    else if(eat(Token::tokenType::MINUS)){
        return new UnaryExprassion('-',primary());
    }
    return primary();
}

Exprassion *Parser::primary(){
    Exprassion* result;
    eat(Token::tokenType::QUOTES);
    Token curToken = get(0);
    if(eat(Token::tokenType::DIGIT)){
        result = new NumberExprassion(curToken.getValue().toDouble());
    }
    else if(eat(Token::tokenType::VAR)){
             result = new VariableExpression(curToken.getValue());
         }
    else if(eat(Token::tokenType::URL)){
            result = new StringExprassion(curToken.getValue());
            consume(Token::tokenType::QUOTES);
}
    else if(eat(Token::tokenType::LBRACKET)){
        result = exprasion();
        consume(Token::tokenType::RBRACKET);
    }
    else{
        throw "Undefined exprassion";
    }
    return result;
}

Parser::Parser(QList<Token> tokens){
    this->tokens = tokens;
    Functions::put("click",new ClickFunction);
    Functions::put("keyPress",new PressFunction);
    Functions::put("runBatFile",new RunFunction);
    Functions::put("drag",new DragFunction);

}

Statement* Parser::parse(){
    BlockStatement* blockStatement = new BlockStatement;
    while(!eat(Token::tokenType::EoF)){
        blockStatement->add(statement());
        eat(Token::tokenType::SEPARATORPC);
    }
    return blockStatement;
}
