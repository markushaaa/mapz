#pragma once
#ifndef PARSER_H
#define PARSER_H
#include <QList>
#include <QTreeWidget>
#include "lexer/token.h"
#include "parser/ast/statement.h"
#include "parser/ast/assignmentstatement.h"
#include "parser/ast/ifelsestatement.h"
#include "parser/ast/whilestatement.h"
#include "parser/ast/blockstatement.h"
#include "parser/ast/breakstatement.h"
#include "parser/ast/functionstatement.h"
#include "parser/ast/exprassion.h"
#include "parser/ast/binaryexprassion.h"
#include "parser/ast/unaryexprassion.h"
#include "parser/ast/conditionalexprassion.h"
#include "parser/ast/variableexpression.h"
#include "parser/ast/numberexprassion.h"
#include "parser/ast/stringexprassion.h"
#include "parser/ast/functionexpression.h"
#include "parser/ast/clickfunction.h"
#include "parser/ast/runfunction.h"
#include "parser/ast/pressfunction.h"
#include "parser/ast/dragfunction.h"
class Parser
{
private:
   QList<Token> tokens;
   int pos = 0;
   Token get(int relPos);
   bool eat(Token::tokenType type);

   Statement* statement();

   Statement* assignmentStatement();


   Exprassion* exprasion();

   Exprassion* additive();

   Exprassion* multiplicative();

   Exprassion* unary();

   Exprassion* primary();

   Statement* ifElse();

   Statement* block();

   Exprassion* condition();

   Statement* function();

   Statement* whileStatement();

   Token consume(Token::tokenType);

public:
    Parser(QList<Token> tokens);

    Statement* parse();

};

#endif // PARSER_H
