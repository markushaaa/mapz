#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnRun_clicked()
{
    ui->teTokens->clear();
    ui->treeWidget->clear();
    lexer = new Lexer(ui->teSource->toPlainText());
    QList<Token>tokens;
    try{
        tokens =  lexer->tokanize();
    }
    catch(char const* e){
        ui->teTokens->clear();
        ui->treeWidget->clear();
        QMessageBox::critical(this,"Lexer error",e);
        return;
    }
     foreach (Token tmp, tokens) {
        ui->teTokens->append(tmp.toString());
    }
    parser = new Parser(tokens);
    Statement* program;
    try{
         program = parser->parse();
    }
    catch(char const* e){
        ui->teTokens->clear();
        ui->treeWidget->clear();
        QMessageBox::critical(this,"Parser error",e);
        return;
    }

    if(ui->optimazeCheck->checkState()){
        optimazer = new Optimazer;
        Statement* newProgram = optimazer->optimaze(program);
        newProgram->toString(ui->treeWidget);
        try {
           newProgram->execute();
        } catch (char const* e) {
            ui->teTokens->clear();
            ui->treeWidget->clear();
            QMessageBox::critical(this,"Runtime error",e);
            return;
        }
    }
    else{
        program->toString(ui->treeWidget);
        program->execute();
    }
}
