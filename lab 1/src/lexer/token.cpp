#include "token.h"


QString Token::typeToString(){
    switch (type) {
    case tokenType::DIGIT: return "DIGIT";
    case tokenType::URL: return "URL";
    case tokenType::IF: return "IF";
    case tokenType::ELSE: return "ELSE";
    case tokenType::WHILE: return "WHILE";
    case tokenType::CLICK: return "CLICK";
    case tokenType::DRAG: return "DRAG";
    case tokenType::PRESS: return "KEYPRESS";
    case tokenType::RUN: return "RUNFILE";
    case tokenType::VAR: return "VAR";
    case tokenType::GS: return "GRATER";
    case tokenType::LS: return "LESS";
    case tokenType::LBRACKET: return "LBRACKET";
    case tokenType::RBRACKET: return "RBRACKET";
    case tokenType::SEPARATORDP: return "SEPARATORDP";
    case tokenType::SEPARATORPC: return "SEPARATORPC";
    case tokenType::QUOTES: return "QUOTES";
    case tokenType::COMA: return "COMA";
    case tokenType::PLUS: return "PLUS";
    case tokenType::MINUS: return "MINUS";
    case tokenType::MUL: return "MUL";
    case tokenType::DIV: return "DIV";
    case tokenType::EoF: return "EOF";
    default:
        "UNDEFINED";
    }
}

Token::Token(Token::tokenType p_type, QString val){
    type = p_type;
    value = val;
}


QString Token::toString(){
    return "type: " +  typeToString() + " || value: '" + value + "'";
}

QString Token::getValue(){
    return value;
}

Token::tokenType Token::getType(){
    return type;
}
