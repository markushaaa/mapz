#include "constantfolding.h"

CoonstantFolding::CoonstantFolding()
{

}

Node *CoonstantFolding::visit(AssignmentStatement *st){
    st->setExpr((Exprassion*)st->getExpr()->accept(this));
    return st;
}

Node *CoonstantFolding::visit(IfElseStatement *st){
    st->setExpr((Exprassion*)st->getExpr()->accept(this));
    st->setIfStatement((Statement*)st->getIfStatement()->accept(this));
    if(st->getElseStatement() != nullptr){
        st->setElseStatement((Statement*)st->getElseStatement()->accept(this));
    }
    return st;
}

Node *CoonstantFolding::visit(BlockStatement *st){
    BlockStatement* newBlock = new BlockStatement;
    foreach(Statement* stm , st->getStatements()){
        stm = (Statement*)stm->accept(this);
        newBlock->add(stm);
    }
    return newBlock;
}

Node *CoonstantFolding::visit(WhileStatement *st){
    st->setCondition((Exprassion*)st->getCondition()->accept(this));
    st->setStatement((Statement*)st->getStatement()->accept(this));
    return st;
}

Node *CoonstantFolding::visit(FunctionStatement *st){
    st->setFunc((Exprassion*)st->getFunc()->accept(this));
    return st;
}

Node *CoonstantFolding::visit(FunctionExpression *st){
    for(int i = 0;i < st->getArguments().length();i++){
        st->setArgument(i,(Exprassion*)st->getArguments()[i]->accept(this));
    }
    return st;
}

Node *CoonstantFolding::visit(BinaryExprassion *st){
    st->setExpr1((Exprassion*)st->getExpr1()->accept(this));
    st->setExpr2((Exprassion*)st->getExpr2()->accept(this));
    if(dynamic_cast<NumberExprassion*>(st->getExpr1()) && dynamic_cast<NumberExprassion*>(st->getExpr2())){
        double result = st->eval()->asNumber();
        return new NumberExprassion(result);
    }
    return st;
}

Node *CoonstantFolding::visit(ConditionalExprassion *st){
    st->setExpr1((Exprassion*)st->getExpr1()->accept(this));
    st->setExpr2((Exprassion*)st->getExpr2()->accept(this));
    Exprassion* expr1 = st->getExpr1();
    Exprassion* expr2 = st->getExpr2();
    if(dynamic_cast<NumberExprassion*>(expr1) && dynamic_cast<NumberExprassion*>(expr2)){
        return new NumberExprassion(st->eval()->asNumber());
    }
    return st;
}

Node *CoonstantFolding::visit(VariableExpression *st){
    return st;
}

Node *CoonstantFolding::visit(UnaryExprassion *st){
    st->getExpr()->accept(this);
    if(dynamic_cast<NumberExprassion*>(st->getExpr())){
        if(st->getOperation() == '-'){
            return new NumberExprassion(-(st->getExpr()->eval()->asNumber()));
        }
        else if(st->getOperation() == '+'){
            return new NumberExprassion(st->getExpr()->eval()->asNumber());
        }
    }
    return st;
}
