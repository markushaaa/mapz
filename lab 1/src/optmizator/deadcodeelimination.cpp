#include "deadcodeelimination.h"

DeadCodeElimination::DeadCodeElimination()
{

}

Node *DeadCodeElimination::visit(AssignmentStatement *st){
    return st;
}

Node *DeadCodeElimination::visit(IfElseStatement *st){
    if(dynamic_cast<NumberExprassion*>(st->getExpr())){
        if(st->getExpr()->eval()->asNumber() != 0){
            return st->getIfStatement();
        }
        else {
            if(st->getElseStatement() != nullptr){
                return st->getElseStatement();
            }
            return nullptr;
        }
    }
    return st;
}

Node *DeadCodeElimination::visit(BlockStatement *st){
    BlockStatement* newBlock = new BlockStatement;
    foreach(Statement* stm, st->getStatements()){
        stm = (Statement*)stm->accept(this);
        if(stm != nullptr){
            newBlock->add(stm);
        }
    }
    return newBlock;
}

Node *DeadCodeElimination::visit(WhileStatement *st){
    if(dynamic_cast<NumberExprassion*>(st->getCondition())){
        if(st->getCondition()->eval()->asNumber() == 0){
            return nullptr;
        }
        else {
            throw std::runtime_error("Looping");
        }
    }
    return st;
}

Node *DeadCodeElimination::visit(FunctionStatement *st){
    return st;
}

Node *DeadCodeElimination::visit(FunctionExpression *st){
    return st;
}

Node *DeadCodeElimination::visit(BinaryExprassion *st){
    return st;
}

Node *DeadCodeElimination::visit(ConditionalExprassion *st){
    return st;
}

Node *DeadCodeElimination::visit(VariableExpression *st){
    return st;
}

Node *DeadCodeElimination::visit(UnaryExprassion *st){
    return st;
}
