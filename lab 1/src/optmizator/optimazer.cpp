 #include "optimazer.h"

Optimazer::Optimazer()
{

}

Statement *Optimazer::optimaze(Statement *notOptimazedProgram){
    CoonstantFolding* cf = new CoonstantFolding;
    Statement* optimazed1 = (Statement*)notOptimazedProgram->accept(cf);
    DeadCodeElimination* dce = new DeadCodeElimination;
    Statement* optimazed2 = (Statement*)optimazed1->accept(dce);
    return optimazed2;
}
