package tamagotchi.models;

import tamagotchi.models.LevelFactory.DifficultyLevelFactory;
import tamagotchi.models.LevelFactory.LevelMenager;
import tamagotchi.models.LevelFactory.TimeMeneger;
import tamagotchi.models.PsevdoServer.Command;
import tamagotchi.models.PsevdoServer.GetCommand;
import tamagotchi.models.PsevdoServer.PsevdoServer;
import tamagotchi.models.food.IFood;
import tamagotchi.ui.GamePanel;
import tamagotchi.ui.ShopPanel;

import javax.swing.*;
import java.util.ArrayList;

public class Engine {
    private Pet pet;
    private DifficultyLevelFactory difficaltyLevelFactory;
    private TimeMeneger timeMeneger;
    private LevelMenager levelMenager;
    private Shop shop;
    private SaveGame sg;
    private static Engine engine;
    private PsevdoServer ps;

    private Command  AddPurchase;

    private Command  DeletePurchase;

    private GetCommand GetPurchases;
    private Command  DeleteAllPurchase;
    public Pet getPet() {
        return pet;
    }

    public DifficultyLevelFactory getDifficaltyLevelFactory() {
        return difficaltyLevelFactory;
    }

    public TimeMeneger getTimeMeneger() {
        return timeMeneger;
    }

    public LevelMenager getLevelMenager() {
        return levelMenager;
    }

    public Shop getShop() {
        return shop;
    }

    public Engine(Pet pet, DifficultyLevelFactory difficaltyLevelFactory, Shop shop) {
        this.pet = pet;
        this.difficaltyLevelFactory = difficaltyLevelFactory;
        this.timeMeneger = this.difficaltyLevelFactory.getTimeMenager();
        this.levelMenager = this.difficaltyLevelFactory.getLevelMenager();
        this.shop = shop;
        ps = new PsevdoServer();

    }

    public SaveGame Save(){
        return new SaveGame(pet,shop,pet.getStorage(),difficaltyLevelFactory);
    }

    public void Load(SaveGame saveGame){
        pet = saveGame.getPet();
        shop = saveGame.getShop();
        pet.setStorage(saveGame.getStorage());
        System.out.println("Load storage = " + saveGame.getStorage().toString());
    }

    public static Engine getInstance(Pet pet, DifficultyLevelFactory difficaltyLevelFactory, Shop shop){
        if(engine == null){
            engine = new Engine(pet,difficaltyLevelFactory,shop);
        }
        return engine;
    }

    public void SetComands(Command addPurchase,Command deletePurchase,GetCommand getPurchases,Command deleteAllPurchase){
        this.AddPurchase = addPurchase;
        this.DeletePurchase = deletePurchase;
        this.GetPurchases = getPurchases;
        this.DeleteAllPurchase = deleteAllPurchase;
    }

    public void AddPurchaseToServer(){
        AddPurchase.execute();
    }

    public void DeletePurchaseFromServer(){
        DeletePurchase.execute();
    }

    public void DeleteAllPurchasesFromServer(){
        DeleteAllPurchase.execute();
    }

    public ArrayList<IFood> GetPurchasesFromServer(){
        GetPurchases.execute();
        return GetPurchases.getPurchaseList();
    }


    public void StartTimer(){
        timeMeneger.setPet(pet);
        timeMeneger.startTimer();
    }

    public PsevdoServer getPs() {
        return ps;
    }

    public void setAddPurchase(Command addPurchase) {
        AddPurchase = addPurchase;
    }

}
