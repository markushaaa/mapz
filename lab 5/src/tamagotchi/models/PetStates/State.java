package tamagotchi.models.PetStates;

import javax.swing.*;
import java.io.Serializable;

public interface State extends Serializable {
    void setImage(ImageIcon image);
}
