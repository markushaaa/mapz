package tamagotchi.models.Observer;

import tamagotchi.models.Shop;
import tamagotchi.models.food.IFood;

import java.io.Serializable;
import java.util.ArrayList;

public interface Observer extends Serializable {
    void handleEvent(Shop shop);
}
