package tamagotchi.models.Observer;

import tamagotchi.models.Shop;
import tamagotchi.models.food.IFood;

import java.util.ArrayList;

public class FoodProvider implements Observer{
    private static final long serialVersionUID = 1L;
    @Override
    public void handleEvent(Shop shop) {
        shop.initializeShop();
    }
}
