package tamagotchi.models.PsevdoServer;

import tamagotchi.models.food.IFood;

public class AddCommand implements Command {
    private PsevdoServer ps;

    private IFood purchase;

    public AddCommand(PsevdoServer ps) {
        this.ps = ps;
    }

    @Override
    public void execute() {
        ps.addPurchase(purchase);
        System.out.println("Add command");
    }

    public void setPurchase(IFood purchase) {
        this.purchase = purchase;
    }
}
