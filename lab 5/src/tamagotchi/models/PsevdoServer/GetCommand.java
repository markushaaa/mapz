package tamagotchi.models.PsevdoServer;

import tamagotchi.models.food.IFood;

import java.util.ArrayList;

public class GetCommand implements Command{
    private PsevdoServer ps;
    private ArrayList<IFood> PurchaseList;

    public GetCommand(PsevdoServer ps) {
        this.ps = ps;
    }

    @Override
    public void execute() {
        PurchaseList = ps.getPurchases();
    }

    public ArrayList<IFood> getPurchaseList() {
        return PurchaseList;
    }
}
