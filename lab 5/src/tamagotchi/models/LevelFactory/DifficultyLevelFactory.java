package tamagotchi.models.LevelFactory;

import java.io.Serializable;

public abstract class DifficultyLevelFactory implements Serializable {
    public abstract LevelMenager getLevelMenager();
    public abstract TimeMeneger getTimeMenager();
}
