package tamagotchi.models.LevelFactory;

import tamagotchi.models.LevelFactory.LevelMenager;

public class HardLevelMeneger implements LevelMenager {
    int xpValue = 0;
    int [] toNextLevel = {500,1000,1500,2000,2500,3000,3500,4000,4500,5000};
    int level;

    @Override
    public int ConvertXpValueToPercent(int xpValue) {
        return xpValue / toNextLevel[level] * 100;
    }

    @Override
    public int ConvertXpValueToLevel(int xpValue) {
        if(xpValue > toNextLevel[level]){
            level++;
            xpValue = 0;
        }
        return level;
    }

    @Override
    public String toString(){
        return "Hard Level";
    }
}
