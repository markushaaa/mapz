package tamagotchi.models.LevelFactory;


import tamagotchi.models.Pet;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EasyTimeMeneger extends Timer implements TimeMeneger{
    private ActionListener actions = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            reduceSatiety();
            reduceHealth();
            increaseXp();
        }
    };
    private int delay = 5000;
    Pet pet;
    public EasyTimeMeneger(){
        super( 0,null);
        setDelay(delay);
        addActionListener(actions);
    }


    @Override
    public void reduceSatiety(){
        pet.changeSatiaty(-5);
        System.out.println("eat = " + pet.getEat());
    }

    @Override
    public void reduceHealth() {
        pet.changeHealth(-5);
        System.out.println("health = " + pet.getHealth());
    }

    @Override
    public void increaseXp() {
        pet.changeXp(5);
        System.out.println("xp = " + pet.getXpPoints());
    }

    @Override
    public void startTimer() {
        super.start();
    }

    @Override
    public void setPet(Pet pet) {
       this.pet = pet;
    }

    @Override
    public String toString(){
        return "Easy timer";
    }
}
