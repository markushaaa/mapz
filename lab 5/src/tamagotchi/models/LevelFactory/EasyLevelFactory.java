package tamagotchi.models.LevelFactory;

import java.io.Serializable;

public class EasyLevelFactory extends DifficultyLevelFactory implements Serializable {
    @Override
    public LevelMenager getLevelMenager() {
        return new EasyLevelMeneger();
    }

    @Override
    public TimeMeneger getTimeMenager() {
        return new EasyTimeMeneger();
    }
}
