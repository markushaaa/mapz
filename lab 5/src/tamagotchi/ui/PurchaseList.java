package tamagotchi.ui;

import tamagotchi.models.Engine;
import tamagotchi.models.food.IFood;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class PurchaseList {
    private JPanel panel1;

    private JButton deleteButton;
    private JButton deleteAllButton;
    private JButton backButton;
    private JList PurchaseList;
    private Engine engine;
    DefaultListModel listModel;

    public PurchaseList(Engine engine) {
        this.engine = engine;
        listModel = new DefaultListModel();
        show();
        PurchaseList.setModel(listModel);

        deleteButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                engine.DeletePurchaseFromServer();
                listModel.clear();
                show();
            }
        });
        deleteAllButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                engine.DeleteAllPurchasesFromServer();
                listModel.clear();
                show();
            }
        });
        backButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(panel1);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                try {
                    topFrame.add(GamePanel.getInstance("Hard","fds",false), BorderLayout.CENTER);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                topFrame.pack();
            }
        });
    }

    private void show(){
        for (IFood food:
                engine.GetPurchasesFromServer()) {
            listModel.addElement(food.toString());
        }
    }

    public JPanel getPanel1() {
        return panel1;
    }
}
