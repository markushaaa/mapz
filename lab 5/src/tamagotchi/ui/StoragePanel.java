package tamagotchi.ui;

import tamagotchi.constants.Constants;
import tamagotchi.models.*;
import tamagotchi.models.LevelFactory.EasyLevelFactory;
import tamagotchi.models.food.IFood;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class StoragePanel extends JPanel {
    private static StoragePanel storagePanel;
    private Storage storage;
    private int startDx = 15;
    private int startDy = 15;
    private Engine engine;
    private ShopButton curButton;
    StoragePanel(){
        setLayout(null);
        engine = Engine.getInstance(Pet.getInstance("fd"),new EasyLevelFactory(),Shop.getInstance());
        storage = engine.getPet().getStorage();
        initializeLayout();
    }

    private void initializeLayout() {
        setPreferredSize(new Dimension(Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT));
    }

    public static StoragePanel getInstance(){
        if(storagePanel == null){
            storagePanel = new StoragePanel();
        }
        return storagePanel;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        startDy = 15;
        startDx = 15;
        getInstance().removeAll();
        getInstance().revalidate();
        for (IFood food : storage.getFoods()){
            ImageIcon icon = food.getImage();
            JLabel lIcon = new JLabel(icon);
            lIcon.setBounds(startDx,startDy,icon.getIconWidth(),icon.getIconHeight());
            StorageButton useButton = new StorageButton(food);
            useButton.setText("Use");
            useButton.setBounds(startDx,startDy + icon.getIconHeight() + 20,60,40);
            add(lIcon);
            add(useButton);
            useButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    StorageButton cButton = (StorageButton)e.getSource();
                    cButton.getFood().use(engine.getPet());
                    storage.removeFood(cButton.getFood());
                    repaint();
                    GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(getInstance());
                }
            });
            startDx += icon.getIconWidth() + 50;
            if(startDx > Constants.BOARD_WIDTH - icon.getIconWidth() - 150){
                startDx = 15;
                startDy += icon.getIconHeight() + useButton.getHeight() + 40;
            }
        }
        JButton backButton = new JButton("Back");
        backButton.setBounds(500,50,100,40);
        add(backButton);
        backButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getInstance().removeAll();
                getInstance().revalidate();
                getInstance().repaint();
                startDx = 15;
                startDy = 15;
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(getInstance());
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                try {
                    topFrame.add(GamePanel.getInstance("gdg","gd",false),BorderLayout.CENTER);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                topFrame.pack();
            }
        });
    }

}

 class StorageButton extends JButton {
    private IFood food;
    StorageButton(IFood food){
        this.food = food;
    }
    IFood getFood(){
        return food;
    }
}

