package tamagotchi.ui;

import tamagotchi.constants.Constants;
import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.*;
import tamagotchi.models.LevelFactory.*;
import tamagotchi.models.PsevdoServer.AddCommand;
import tamagotchi.models.PsevdoServer.DeleteAllCommand;
import tamagotchi.models.PsevdoServer.DeleteCommand;
import tamagotchi.models.PsevdoServer.GetCommand;
import tamagotchi.models.food.Food;
import tamagotchi.models.food.HealthyFoodDecorator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.IOException;
import java.text.AttributedString;

public class GamePanel extends JPanel {
    private Timer gameTimer;
    private static GamePanel gamePanel;
    Engine engine;
    JLabel lbShop;
    JLabel lbStorage;
    JLabel lbSave;
    JLabel lbList;
    String diffculty;
    JPanel thisP = this;


    private GamePanel(String diffculty,String name,boolean isLoad) throws IOException, ClassNotFoundException {
        JPanel thisP = this;
        setLayout(null);
        this.diffculty = diffculty;
        initializeLayout();
        initializeVariables(name,isLoad);
    }

    public static GamePanel getInstance(String diffculty,String name,boolean isLoad) throws IOException, ClassNotFoundException {
        if(gamePanel == null){
            gamePanel = new GamePanel(diffculty,name,isLoad);
        }
        return gamePanel;
    }

    private void initializeVariables(String petName,boolean isLoad) throws IOException, ClassNotFoundException {
        gameTimer = new Timer(Constants.GAME_SPEED, new GameLoop(this));
        DifficultyLevelFactory dlf;
        if (diffculty == "Hard") {
            dlf = new HardLevelFactory();
        } else {
            dlf = new EasyLevelFactory();
        }
        if(isLoad){
            SaveGame sg = SaveGame.Deserialize("D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\save\\save.txt");
            engine = Engine.getInstance(sg.getPet(),dlf,sg.getShop());
            engine.SetComands(new AddCommand(engine.getPs()),new DeleteCommand(engine.getPs()),
                    new GetCommand(engine.getPs()),new DeleteAllCommand(engine.getPs()));
        }
        else {
            engine = Engine.getInstance(Pet.getInstance(petName), dlf, Shop.getInstance());
            engine.SetComands(new AddCommand(engine.getPs()),new DeleteCommand(engine.getPs()),
                    new GetCommand(engine.getPs()),new DeleteAllCommand(engine.getPs()));
        }
        lbShop = new JLabel(Shop.shopImage);
        lbShop.setBounds(Shop.dx, Shop.dy, Shop.width, Shop.height);
        lbStorage = new JLabel(Storage.storageImage);
        lbStorage.setBounds(Storage.dx, Storage.dy, Storage.width, Storage.height);
        ImageIcon saveImage = ImageFactory.getImage(Image.SAVE);
        lbSave = new JLabel(saveImage);
        lbSave.setBounds(130,85,saveImage.getIconWidth(),saveImage.getIconHeight());
        ImageIcon listImage = ImageFactory.getImage(Image.LIST);
        lbList = new JLabel(listImage);
        lbList.setBounds(550,0,listImage.getIconWidth(),listImage.getIconHeight());
        gameTimer.start();
        engine.StartTimer();
        lbShop.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(thisP);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                topFrame.add(ShopPanel.getInstance(),BorderLayout.CENTER);
                topFrame.pack();
            }
        });
        lbStorage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(thisP);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                topFrame.add(StoragePanel.getInstance(),BorderLayout.CENTER);
                topFrame.pack();
            }
        });
        lbSave.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SaveGame sg = engine.Save();
                try {
                    sg.Serialize();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                System.out.println(sg.toString());
            }
        });

        lbList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(thisP);
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                PurchaseList pl = new PurchaseList(Engine.getInstance(Pet.getInstance("fd"),new EasyLevelFactory(),Shop.getInstance()));
                topFrame.add(pl.getPanel1(),BorderLayout.CENTER);
                topFrame.pack();
            }
        });

    }
    private void initializeLayout() {
        setPreferredSize(new Dimension(Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(ImageFactory.getImage(Image.ROOM).getImage(),0,0,Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT,null);
        engine.getPet().checkState();
        g.drawImage(Pet.petImage.getImage(),240,240,null);
        g.drawImage(ImageFactory.getImage(Image.DESK).getImage(),130,0,null);
        g.drawImage(ImageFactory.getImage(Image.LEVEL_STATUS).getImage(),150,15,null);
        g.drawImage(ImageFactory.getImage(Image.EAT_STATUS).getImage(),240,5,null);
        g.drawImage(ImageFactory.getImage(Image.HP_STATUS).getImage(),350,5,null);
        g.drawImage(ImageFactory.getImage(Image.GOLD).getImage(),440,5,null);
        add(lbShop);
        add(lbStorage);
        add(lbSave);
        add(lbList);
        Font font = new Font("LucidaSans", Font.BOLD, 20);
        AttributedString atPetNameString= new AttributedString(engine.getPet().getName());
        atPetNameString.addAttribute(TextAttribute.FONT, font);

        Font levelFont = new Font("LucidaSans", Font.BOLD,13);
        AttributedString atLevelString= new AttributedString(String.valueOf(engine.getLevelMenager().ConvertXpValueToLevel(engine.getPet().getXpPoints())));
        atLevelString.addAttribute(TextAttribute.FONT, levelFont);

        Font statsFont = new Font("LucidaSans", Font.BOLD,12);
        AttributedString atEatString= new AttributedString(String.valueOf(engine.getPet().getEat()) + "%");
        atEatString.addAttribute(TextAttribute.FONT, statsFont);

        AttributedString atHealthString= new AttributedString(String.valueOf(engine.getPet().getHealth()) + "%");
        atHealthString.addAttribute(TextAttribute.FONT, statsFont);

        AttributedString atGoldString= new AttributedString(String.valueOf(engine.getPet().getGold()));
        atGoldString.addAttribute(TextAttribute.FONT, statsFont);
        g.drawString(atPetNameString.getIterator(),270,120);
        g.drawString(atLevelString.getIterator(),176,48);
        g.drawString(atEatString.getIterator(),230,64);
        g.drawString(atHealthString.getIterator(),340,64);
        g.drawString(atGoldString.getIterator(),440,64);
}

    public void doOneLoop() {
        update();
        repaint();
    }
    private void update(){

    }

}
