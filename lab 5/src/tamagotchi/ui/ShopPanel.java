package tamagotchi.ui;

import tamagotchi.constants.Constants;
import tamagotchi.models.Engine;
import tamagotchi.models.LevelFactory.EasyLevelFactory;
import tamagotchi.models.Pet;
import tamagotchi.models.Shop;
import tamagotchi.models.ShopFacade;
import tamagotchi.models.food.Food;
import tamagotchi.models.food.IFood;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class ShopPanel extends JPanel {
    private static ShopPanel shopPanel;
    private Shop shop;
    Engine engine;
    private int startDx = 15;
    private int startDy = 15;
    private ShopButton curButton;
    ShopPanel(){
        setLayout(null);
        engine = Engine.getInstance(Pet.getInstance("fd"),new EasyLevelFactory(),Shop.getInstance());
        shop = engine.getShop();
        initializeLayout();
    }

    private void initializeLayout() {
        setPreferredSize(new Dimension(Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT));
    }

    public static ShopPanel getInstance(){
        if(shopPanel == null){
            shopPanel = new ShopPanel();
        }
        return shopPanel;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        startDy = 15;
        startDx = 15;
        getInstance().removeAll();
        getInstance().revalidate();
        for (IFood food : shop.getFoods()){
            ImageIcon icon = food.getImage();
            JLabel lIcon = new JLabel(icon);
            lIcon.setBounds(startDx,startDy,icon.getIconWidth(),icon.getIconHeight());
            ShopButton buyButton = new ShopButton(food);
            buyButton.setText("Buy");
            buyButton.setBounds(startDx,startDy + icon.getIconHeight() + 20,60,40);
            add(lIcon);
            add(buyButton);
            buyButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    ShopButton cButton = (ShopButton)e.getSource();
                    ShopFacade facade = new ShopFacade();
                    GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(getInstance());
                    try {
                        facade.Buy(topFrame,cButton.getFood());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace();
                    }
                    repaint();
                }
            });
            startDx += icon.getIconWidth() + 50;
            if(startDx > Constants.BOARD_WIDTH - icon.getIconWidth() - 150){
                startDx = 15;
                startDy += icon.getIconHeight() + buyButton.getHeight() + 40;
            }
        }
        JButton backButton = new JButton("Back");
        backButton.setBounds(500,50,100,40);
        add(backButton);
        backButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                startDx = 15;
                startDy = 15;
                GameMainFrame topFrame = (GameMainFrame) SwingUtilities.getWindowAncestor(getInstance());
                topFrame.getContentPane().removeAll();
                topFrame.repaint();
                try {
                    topFrame.add(GamePanel.getInstance("gdg","gd",false),BorderLayout.CENTER);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                topFrame.pack();
            }
        });
    }

}

 class ShopButton extends JButton {
    private IFood food;
    ShopButton(IFood food){
        this.food = food;
    }
    IFood getFood(){
        return food;
    }
}