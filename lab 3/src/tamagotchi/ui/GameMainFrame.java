package tamagotchi.ui;

import tamagotchi.constants.Constants;
import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.Shop;
import tamagotchi.models.food.Food;
import tamagotchi.models.food.HealthyFoodDecorator;
import tamagotchi.models.food.IFood;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class GameMainFrame extends JFrame {
    private JPanel gamePanel;
    private JPanel shopPanel;
    private JPanel storagePanel;
    private JPanel startPanel;

    public GameMainFrame() throws IOException, ClassNotFoundException {
        initializeLayout();
    }

        private  void initializeLayout() throws IOException, ClassNotFoundException {
        LevelChoose levelChoose = new LevelChoose();
        File saveFile = new File("D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\save\\save.txt");
            System.out.println(saveFile.length());
        if(saveFile.length() != 0){
            add(GamePanel.getInstance("fsd","fds",true), BorderLayout.CENTER);
        }
        else {
            add(new LevelChoose().getPanel1(), BorderLayout.CENTER);
        }
        pack();
        setTitle(Constants.TITLE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }


    public void setGamePanel(JPanel gamePanel) {
        gamePanel = gamePanel;
    }

    public void setShopPanel(JPanel shopPanel) {
        shopPanel = shopPanel;
    }

    public void setStoragePanel(JPanel storagePanel) {
        storagePanel = storagePanel;
    }
}

