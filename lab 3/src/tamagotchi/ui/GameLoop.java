package tamagotchi.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameLoop implements ActionListener {
    private GamePanel gamePanel;

    public GameLoop(GamePanel panel){
        gamePanel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        gamePanel.doOneLoop();
    }
}
