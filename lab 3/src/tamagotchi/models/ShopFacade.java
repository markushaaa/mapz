package tamagotchi.models;

import tamagotchi.models.LevelFactory.EasyLevelFactory;
import tamagotchi.models.PsevdoServer.AddCommand;
import tamagotchi.models.food.Food;
import tamagotchi.models.food.IFood;
import tamagotchi.ui.GameMainFrame;
import tamagotchi.ui.GamePanel;

import javax.swing.*;
import java.io.IOException;

public class ShopFacade {
    public void Buy(GameMainFrame frame, IFood food) throws IOException, ClassNotFoundException {
//        Food cFood = (Food)food;
//        if(cFood.getPrice() > Pet.getInstance("Fds").getGold()){
//            return;
//        }
        Engine engine = Engine.getInstance(Pet.getInstance("fds"),new EasyLevelFactory(),Shop.getInstance());
        Shop shop = engine.getShop();
        Storage storage = engine.getPet().getStorage();
        AddCommand ac = new AddCommand(engine.getPs());
        ac.setPurchase(food);
        engine.setAddPurchase(ac);
        engine.AddPurchaseToServer();
        shop.removeFood(food);
        if(shop.isEmpty()){
            shop.notifyObservers();
        }
        if(storage.isFreeSpace()){
            storage.addFood(food);
        }
        frame.setGamePanel(GamePanel.getInstance("gfdgfd","fds",false));
    }
}
