package tamagotchi.models;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.LevelFactory.DifficultyLevelFactory;

import javax.swing.*;
import java.io.*;

public class SaveGame implements Serializable {
    private static final long serialVersionUID = 1L;
    private final Pet pet;
    private final Shop shop;
    private final Storage storage;

    public SaveGame(Pet pet, Shop shop, Storage storage,DifficultyLevelFactory dlf) {
        this.pet = pet;
        this.shop = shop;
        this.storage = storage;

    }

    public static SaveGame Deserialize(String filePath) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        SaveGame savedGame = (SaveGame) objectInputStream.readObject();
        System.out.println(savedGame);
        objectInputStream.close();
        return savedGame;
    }

    public void Serialize() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\2 course labs\\2 semester\\MPZ\\TamoApp\\src\\tamagotchi\\save\\save.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(this);
        objectOutputStream.close();
        System.out.println(this);
    }

    public Pet getPet() {
        return pet;
    }

    public Shop getShop() {
        return shop;
    }

    public Storage getStorage() {
        return storage;
    }


    @Override
    public String toString() {
        return "SaveGame{" +
                "pet=" + pet.toString() +
                ", shop=" + shop +
                ", storage= " + storage.toString() +
                '}';
    }


}
