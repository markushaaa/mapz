package tamagotchi.models.LevelFactory;

import tamagotchi.models.LevelFactory.LevelMenager;

public class EasyLevelMeneger implements LevelMenager {

    int [] toNextLevel = {100,200,300,400,500,600,700,800,900,1000};
    int level;

    @Override
    public int ConvertXpValueToPercent(int xpValue) {
        return xpValue / toNextLevel[level] * 100;
    }

    @Override
    public int ConvertXpValueToLevel(int xpValue) {
        if(xpValue > toNextLevel[level]){
            level++;
            xpValue = 0;
        }
        return level;
    }

    @Override
    public String toString(){
        return "Easy level";
    }

}
