package tamagotchi.models.food;

import tamagotchi.models.Pet;

import javax.swing.*;
import java.io.Serializable;

public class BaseFoodDecorator implements IFood, Serializable {
    private static final long serialVersionUID = 1L;
    private Food food;

    public BaseFoodDecorator(Food food){
        this.food = food;
    }

    @Override
    public void use(Pet pet) {
        food.use(pet);
    }

    @Override
    public ImageIcon getImage() {
        return food.getImage();
    }

    @Override
    public IFood clone() {
        return food.clone();
    }

    @Override
    public String toString() {
        return food.toString();
    }
}
