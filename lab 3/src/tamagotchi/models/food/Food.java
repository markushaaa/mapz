package tamagotchi.models.food;

import tamagotchi.models.Pet;

import javax.swing.*;

public class Food implements IFood{
    private ImageIcon image;

    public String getName() {
        return name;
    }

    private String name;
    private int calories;
    private int price;
    public Food(ImageIcon image,String name,int calories,int price){
        this.image = image;
        this.name = name;
        this.calories = calories;
        this.price = price;
    }

    public Food(Food food){
        this.image = food.image;
        this.calories = food.calories;
        this.price = food.price;
        this.name = name;
    }

    @Override
    public void use(Pet pet)
    {
        if(pet.getEat() + calories > 100){
            pet.changeSatiaty(100 - pet.getEat());
            return;
        }
        pet.changeSatiaty(calories);
    }

    public ImageIcon getImage() {
        return image;
    }

    public int getPrice(){
        return price;
    }

    @Override
    public IFood clone() {
        return new Food(this);
    }

    @Override
    public String toString() {
        return "Food{" +
                "name='" + name + '\'' +
                ", calories=" + calories +
                ", price=" + price +
                '}';
    }
}
