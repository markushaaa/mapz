package tamagotchi.models.PetStates;

import tamagotchi.images.Image;
import tamagotchi.images.ImageFactory;
import tamagotchi.models.Pet;

import javax.swing.*;

public class FunnyState implements State{
    @Override
    public void setImage(ImageIcon image) {
        Pet.petImage = ImageFactory.getImage(Image.PET);
    }
}
